#######################################################
# Exercise-1:
# Fill in the following methods:
# Good Luck!

# 1
def biggest(a,b,c)
  # Return the maximum number among a, b and child
  [a,b,c].max

end

# 2
def longest(a,b,c)
  # Return an array with the string as first item, and length as second item
  longest_var = [a,b,c].max{|a, b| a.length <=> b.length}
  ret_val = [longest_var,longest_var.length]
end

# 3
def age_category(birth_year)
  # Return the "child" if age is smaller 18, "adult" if age is between 18 and 64, and "senior" if age is 65 or more
  # You can assume the current year is 2013 (or research a way to retrieve it)
  # if birth_year is in the future - returns nil
  curr_year = Time.now.strftime("%Y").to_i
  user_age = curr_year - birth_year

  case user_age
  when 0..18
    user_age_category = 'child'
  when 18..64
    user_age_category = "adult"
  when 64..120
    user_age_category = "senior"
  # Beware of vampires, in case you
  # meet one, get yourself some garlic  
  when user_age > 120
    user_age_category = "immortal"     
  end    



end

# 4
def full_name(first_name, middle_name = nil, last_name = nil)
  # Return the capitalized full name, according to the names of a person
  # Each of the names might be nil

  #works only for English, for other languages
  # use "unicode"
  ret_name = first_name.capitalize
  unless middle_name.nil?
    ret_name << " " + middle_name.capitalize 
  end

  unless last_name.nil?
    ret_name << " " + last_name.capitalize
  end

  return ret_name
end

# 5
def find(arr, value)
  # find if a value (string or symbol) is inside an array (of strings and symbols).
  # The method is indifferent to symbols and strings

  symbols = arr.map { |s| s.to_sym }
  inc = arr.include? value
  inc |= symbols.include? value
  inc |= arr.include? value.to_sym

end

###################################################################
# The following methods test the above methods.
# Don't change them:
def test_biggest
  biggest(2, 3, 4)  == 4 &&
  biggest(4, 3, 2)  == 4 &&
  biggest(2, 4, 3)  == 4
end

def test_longest
  longest("my" , "name" , "is") == ["name", 4] &&
  longest("name" , "is", "my")  == ["name", 4] &&
  longest("is" , "my", "name")  == ["name", 4]
end

def test_age_category
  age_category(2000) == "child"  &&
  age_category(1925) == "senior" &&
  age_category(2022) == nil      &&
  age_category(1982) == "adult"
end


def test_full_name
  full_name("george", "w.", "bush") == "George W. Bush" &&
  full_name("bill", nil, "clinton") == "Bill Clinton" &&
  full_name("bill", "clinton")      == "Bill Clinton" &&
  full_name("madonna") == "Madonna"
end

def test_find
  find([:admin, :operator], 'operator') == true &&
  find(['admin', 'operator'], :operator) == true
end

##
# run tests:
%w(biggest longest age_category full_name find).each do |name|
  puts "Exercise #{name} is " + (send("test_#{name}") ? "correct" : "incorrect")
end
