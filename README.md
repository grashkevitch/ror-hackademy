Ruby on Rails 20131014 Hackademy course
======================================================

Logistics
-------------------
* The course is conducted in [Hashavit 8 street in Ramat Gan](https://maps.google.co.il/maps?q=%D7%91%D7%A6%D7%9C%D7%90%D7%9C+8+%D7%A8%D7%9E%D7%AA+%D7%92%D7%9F&hl=en&ie=UTF8&sll=31.406252,35.081816&sspn=8.07825,14.27124&hnear=Bezalel+8,+Ramat+Gan&t=m&z=17&iwloc=A)
* [Contact list](https://docs.google.com/spreadsheet/ccc?key=0AjmoGsEvx7-gdDhJN25adnFxOUN2ZTZoYmVndF9zTkE&usp=sharing#gid=0)

Presentations
-------------------
* [Course Overview](http://slid.es/hackademy/1-course-overview?token=7bHqpAVJzXqazqSsyLpabdfac4sr)
* [Intro to Web Dev](http://slid.es/hackademy/2-intro-to-web-with-ruby-on-rails?token=4tBeAWaU4PAJymmKykiAbrBQan8r)
* [Shell](http://slid.es/hackademy/3-shell?token=v5Q7CQN1ye87TXX62RBLh82p3JUr)
* [Ruby Intro](http://slid.es/hackademy/4-ruby-intro?token=1R4pRiqrkHTiMQ7QBykzofrcYMgr)

Exersizes
-------------------
* [Hackademy Exercises Bitbucket Repo](https://bitbucket.org/500tech/hackademy-exercises)
* [Ruby training](http://koans.heroku.com/en)
